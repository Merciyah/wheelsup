
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ListView,
  Image,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Nav from './widgets/nav'
import Flights from "../mockdata/flights"
import CallToAct from './widgets/calltoAct'

const {height, width} = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class login extends Component {
  constructor(props){
    super(props)

    this.state = {
      username:'',
      Password:'',
      dataSource: ds.cloneWithRows(Flights),
    }
  }
  eachFlight(x){
    return(<View style={{flex:1,margin:1, backgroundColor:'#555', justifyContent:'center', alignItems:'center'}}>
      <Text style={{color:'#fff', fontWeight:'600', fontSize:17}}>{x.time}</Text>
      <Text style={{color:'#fff', fontWeight:'300', fontSize:11}}>{x.message}</Text>
      </View>)
  }
  render() {
    return (
     <View style={styles.container}>
     <Nav name="YOUR FLIGHT" />
     <View style={{flex:3, flexDirection:'row'}}>
     <Image source={this.props.departures.image} resizeMode="cover" style={{height:null, width:null, backgroundColor:'#fff', flex:1, justifyContent:'center', alignItems:'center'}}>
     <Text style={{color:'#fff', backgroundColor:'rgba(0,0,0,0)', fontSize:22, fontWeight:'300'}}>{this.props.departures.short}</Text>
     </Image>
     
     <Image source={this.props.destination.image} resizeMode="cover" style={{height:null, width:null, backgroundColor:'#666', flex:1, justifyContent:'center', alignItems:'center'}}>
     <Text style={{color:'#fff', backgroundColor:'rgba(0,0,0,0)', fontSize:22, fontWeight:'300'}}>{this.props.destination.short}</Text>
     </Image>
     <View style={{position:'absolute', left:width/2-25, borderRadius:25, backgroundColor:'#fff', padding:10, transform:[{rotate:'90deg'}], top:50}} >
     <Icon name = "local-airport" size={30} color='rgba(0,0,0,0.7)' style={{backgroundColor:"rgba(0,0,0,0)"}}/>
     </View>
     </View>
       <View style={{flex:1, margin:1, flexDirection:'row', backgroundColor:'#555', justifyContent:'space-between', }}>
      <TouchableOpacity style={{backgroundColor:'#6a6a6a',flex:1, flexDirection:'row', margin:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
      <Icon name = "insert-invitation" size={17} color="#fff" />
      <Text style={{color:'#fff', padding:5, fontSize:12, fontWeight:'200'}}>MARCH 23 2017</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{backgroundColor:'#6a6a6a',flex:1, flexDirection:'row', margin:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
       <Icon name = "insert-invitation" size={17} color="#fff" />
      <Text style={{color:'#fff', padding:5, fontSize:12, fontWeight:'200'}}>AUGUST 20 2017</Text>
      </TouchableOpacity>
      </View>
      <View style={{flex:5, flexDirection:'row'}}>
   <ListView 
      dataSource = {this.state.dataSource}
      style={{flex:1}}
      contentContainerStyle={{flex:1}}
      renderRow = {(data) => this.eachFlight(data)}
      />
        <ListView 
      dataSource = {this.state.dataSource}
      style={{flex:1}}
      contentContainerStyle={{flex:1}}
      renderRow = {(data) => this.eachFlight(data)}
      />
    </View>
     <View style={{flex:1, flexDirection:'row'}}>
     <View style={{backgroundColor:'#666', margin:1, justifyContent:'center', flex:1, alignItems:'center'}}>
     <Text style={{color:'#fff'}}> FIRST CLASS </Text>
     </View>
     <View style={{backgroundColor:'#666', margin:1, justifyContent:'center', flex:1, alignItems:'center'}}>
     <Text style={{color:'#fff'}}> 3 Adults </Text>
     </View>
     </View>
     <View style={{flex:1}}>
      <View style={{backgroundColor:'#666', margin:1, justifyContent:'center', flex:1, alignItems:'center'}}>
     <Text style={{color:'#fff', fontSize:18}}> $11,650 </Text>
     </View></View>
     <View style={{flex:1}}>
      <TouchableOpacity style={{height:50, flexDirection:'row', paddingBottom:0, backgroundColor:'#24a4d3', justifyContent:'center', alignItems:'center'}} >

      <Text style={{color:'#fff', fontWeight:'400', fontSize:18, textAlign:'center'}}>All Set!</Text>

      </TouchableOpacity>
     </View>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:null,
    height:null,
    backgroundColor:'#444'


  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
