
import React, { Component } from 'react';
import {

  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ListView,
  View,

} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Nav from './widgets/nav'
import CallToAct from './widgets/calltoAct'
import Cities from "../mockdata/cities"



const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class Location extends Component {
  constructor(props){
    super(props)
    console.log(this.props)

    this.state = {
      to:"",
      from:'',
      dataSource: ds.cloneWithRows(Cities),
    }
  }

  nextLocation(x){

    if(this.props.next.short == "destination"){
      this.props.setDept(x)
      this.props.navigator.push({id:"location"})
    }else{
     this.props.setDest(x)
      this.props.navigator.push({id:"search"}) 
    }
  }

  city(x){
    return(
      <TouchableOpacity onPress={() => this.nextLocation(x)} style={{flexDirection:"row", height:60, justifyContent:'space-between', padding:10, borderBottomWidth:1, borderColor:'#333', alignItems:'center'}}>
      <View style={{flexDirection:"row", alignItems:"center"}}>
      <Text style={{color:'#fff', fontSize:18}}>{x.name}</Text>
      <Text style={{color:'#e7e7e7', fontSize:13}}> ({x.country})</Text>
      </View>
            <Text style={{color:'#e7e7e7', fontSize:13}}> {x.short}</Text>

      </TouchableOpacity>
      )
  }

  render() {
    return (
      <View style={styles.container}>
     <Nav name="SEARCH" />
     <CallToAct name={this.props.next.short.toUpperCase()} />
     <View style={{height:50, padding:8, flexDirection:'row', alignItems:'center', backgroundColor:'#fff'}}>
     <TextInput 
      value = {this.state.to}
      onChangeText={(to) => this.setState({to})}
      style={{flex:9}}
      placeholder = "Destination"
      />
      <Icon name="search" color="#333" size={20} />
      </View>
      <ListView 
      dataSource = {this.state.dataSource}
      renderRow = {(data) => this.city(data)}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#444'


  }
});
