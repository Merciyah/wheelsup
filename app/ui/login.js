
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class login extends Component {
  constructor(props){
    super(props)

    this.state = {
      username:'',
      Password:''
    }
  }
  render() {
    return (
      <Image source={require('../assets/images/back.jpg')} resizeMode="stretch" style={styles.container}>
      <View style={{flex:1}} />
      <View style={{flex:3, justifyContent:'center'}} >
      <Image source={require('../assets/images/logo1.png')} style={{alignSelf:'center'}} resizeMode="contain" />
      </View>
      <View style={{flex:3, }} >
      <View style={{margin:20, justifyContent:'flex-end', flex:1, backgroundColor:'rgba(255,255,255,0.26)'}}>
      <TouchableOpacity onPress={() => this.props.navigator.push({id:'search'})}>
      <Text style={{color:'#fff', alignSelf:'center', margin:25, fontSize:16}}>SIGN IN</Text>
      </TouchableOpacity>
            <View style={{top:-10, flexDirection:'row', alignItems:'center', left:-10, width:330, height:65, position:'absolute', backgroundColor:'rgba(0,0,0,0.6)'}}>
            <Icon name="person" size={25} color="#eee" style={{margin:10}} />
            <TextInput 
              value = {this.state.username}
              onChangeText={(username) => this.setState({username})}
              placeholder = "Username"
              placeholderTextColor ='#fff'
              style = {{flex:1, color:'#ddd', margin:10}}
            />
            </View>
            <View style={{top:70, left:-10, alignItems:'center', flexDirection:'row', width:330, height:65, position:'absolute', backgroundColor:'rgba(0,0,0,0.6)'}} >
            <Icon name="lock" size={25} color="#eee" style={{margin:10}} />
            <TextInput 
              value = {this.state.pass}
              onChangeText={(pass) => this.setState({pass})}
              placeholder = "Password"
              placeholderTextColor ='#fff'
              secureTextEntry = {true}
              style = {{flex:1, color:'#ddd', margin:10}}
            />

      </View>
      </View>
      </View>
      <View style={{flex:1}} />
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:null,
    height:null,


  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
