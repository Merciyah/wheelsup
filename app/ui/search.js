
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  View,
  StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

  var seatTypes = ["TOURIST", "BUSINESS", "FIRST CLASS"]
  const oneway = "One Way";
  const roundtrip = "Round Trip";

export default class search extends Component {
  constructor(props){
    super(props)
    console.log(this.props)
    this.state = {
      adults:1,
      children:0,
      infants:0,
      seatType:0,
      toAndfrom:false,
      to:"",
      from:'',
      tripType:roundtrip
    }
  }


    toAndfrom(){
      if(this.props.departures == ""){
return( 
          <TouchableOpacity onPress={() => this.props.navigator.push({id:'location'})} style={{flex:6}}>
          <Image style={{flex:6, alignItems:'center', justifyContent:'space-around', width:null, height:null}} resizeMode="stretch" source={require('../assets/images/maps.jpg')} >
      <Text style={{color:'#fff', backgroundColor:'rgba(0,0,0,0)', fontSize:23, fontWeight:'200'}}>DEPARTURES</Text>
      <View style={{backgroundColor:'#fff', padding:10, borderRadius:25}}>
      <Icon name="local-airport" size={30} color="#444" style={{backgroundColor:'rgba(0,0,0,0)'}} />
      </View>
      <Text style={{color:'#fff', backgroundColor:'rgba(0,0,0,0)', fontSize:23, fontWeight:'200'}}>DESTINATION</Text>
      </Image>
      </TouchableOpacity>
      )
      }else{
        return( 
          <TouchableOpacity onPress={() => this.props.navigator.push({id:'location'})} style={{flex:6, backgroundColor:'#222'}}>
          <Image style={{flex:6, width:null, height:null}} resizeMode="stretch" source={this.props.departures.image} >
      <View style={{flex:1, backgroundColor:'rgba(0,0,0,0.4)', alignItems:'center', justifyContent:'space-around',}}>
      <Text style={{color:'#fff', backgroundColor:'rgba(0,0,0,0)', fontSize:23, fontWeight:'200'}}>{this.props.departures.name.toUpperCase()}</Text>
      </View>
      </Image>
      <Image style={{flex:6, width:null, height:null}} resizeMode="stretch" source={this.props.destination.image} >
            <View style={{flex:1, backgroundColor:'rgba(0,0,0,0.4)', alignItems:'center', justifyContent:'space-around',}}>
      <Text style={{color:'#fff', backgroundColor:'rgba(0,0,0,0)', fontSize:23, fontWeight:'200'}}>{this.props.destination.name.toUpperCase()}</Text>
      </View>
      </Image>
      </TouchableOpacity>
      )
      }
    }
    seatType(x){
    if(seatTypes[this.state.seatType] == x){
return(<TouchableOpacity style={{flex:1, alignItems:'center',margin:2,borderWidth:1, borderColor:'#888', backgroundColor:'rgba(255,255,255,0.9)', justifyContent:'center'}}>
      <Text style={{color:'#2e99d1',  padding:5}}>{x.toUpperCase()}</Text>
      </TouchableOpacity>)
    }else{
    return(<TouchableOpacity onPress = {() => this.setState({seatType: seatTypes.indexOf(x)})} style={{flex:1, alignItems:'center',margin:2,borderWidth:1, borderColor:'#444', justifyContent:'center'}}>
      <Text style={{color:'#fff', padding:5}}>{x.toUpperCase()}</Text>
      </TouchableOpacity>)
  }
  }

  flightType(x){
    if(this.state.tripType == x){
return(<TouchableOpacity style={{flex:1, alignItems:'center', backgroundColor:'#fff', justifyContent:'center'}}>
      <Text style={{color:'#2e99d1', padding:5}}>{x.toUpperCase()}</Text>
      </TouchableOpacity>)
    }else{
    return(<TouchableOpacity onPress={() => this.setState({tripType:x})} style={{flex:1, alignItems:'center', justifyContent:'center'}}>
      <Text style={{color:'#fff', padding:5}}>{x.toUpperCase()}</Text>
      </TouchableOpacity>)
  }
  }
  render() {
    return (
      <View style={styles.container}>
      <StatusBar
     barStyle="light-content"
   />
      <View style={{flex:1, margin:1, flexDirection:'row', padding:10, paddingBottom:0, backgroundColor:'#222', justifyContent:'space-between', alignItems:'center'}} >
      <Icon name="sort" color="#eee" size={25} />
      <Text style={{color:'#fff', fontWeight:'300', fontSize:14}}>SEARCH FLIGHTS</Text>
      <View style={{width:25}} />
      </View>
      <View style={{flex:1, margin:1, flexDirection:'row', backgroundColor:'#444', justifyContent:'space-between', }}>
      {this.flightType(oneway)}
      {this.flightType(roundtrip)}
      </View>
     {this.toAndfrom()}
      <View style={{flex:6}}>
      <View style={{flex:1, margin:1, flexDirection:'row', backgroundColor:'#555', justifyContent:'space-between', }}>
      <TouchableOpacity style={{backgroundColor:'#6a6a6a',flex:1, flexDirection:'row', margin:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
      <Icon name = "insert-invitation" size={17} color="#fff" />
      <Text style={{color:'#fff', padding:5, fontSize:12, fontWeight:'200'}}>DEPARTURE DATE</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{backgroundColor:'#6a6a6a',flex:1, flexDirection:'row', margin:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
       <Icon name = "insert-invitation" size={17} color="#fff" />
      <Text style={{color:'#fff', padding:5, fontSize:12, fontWeight:'200'}}>RETURN DATE</Text>
      </TouchableOpacity>
      </View>
      <View style={{flex:2, margin:1, flexDirection:'row', backgroundColor:'#555', justifyContent:'space-between', }}>
      <View style={styles.eachCat}>
      <View style={styles.catName}>
      <Text style={styles.main}>
      ADULT
      </Text>
      <Text style={styles.catSub}>
      +12 years 
      </Text>
      </View>
      <View style={{alignItems:'center'}}>
      <TouchableOpacity onPress={() => this.setState({adults:this.state.adults+1})}>
      <Icon name="expand-less" color="#fff" size={25} />
      </TouchableOpacity>
      <Text style={{color:'#fff'}}>{this.state.adults}</Text>
      <TouchableOpacity onPress={() => this.setState({adults:this.state.adults-1})}>
      <Icon name="expand-more" color="#fff" size={25} />
      </TouchableOpacity>
      </View>
      </View>
        <View style={styles.eachCat}>
      <View style={styles.catName}>
      <Text style={styles.main}>
      CHILD
      </Text>
      <Text style={styles.catSub}>
      2 - 12 years 
      </Text>
      </View>
      <View style={{alignItems:'center'}}>
      <TouchableOpacity onPress={() => this.setState({children:this.state.children+1})}>
      <Icon name="expand-less" color="#fff" size={25} />
      </TouchableOpacity>
      <Text style={{color:'#fff'}}>{this.state.children}</Text>
      <TouchableOpacity onPress={() => this.setState({children:this.state.children-1})}>
      <Icon name="expand-more" color="#fff" size={25} />
      </TouchableOpacity>
      </View>
      </View>
        <View style={styles.eachCat}>
      <View style={styles.catName}>
      <Text style={styles.main}>
      INFANT
      </Text>
      <Text style={styles.catSub}>
      -2 years 
      </Text>
      </View>
      <View style={{alignItems:'center'}}>
      <TouchableOpacity onPress={() => this.setState({infants:this.state.infants+1})}>
      <Icon name="expand-less" color="#fff" size={25} />
      </TouchableOpacity>
      <Text style={{color:'#fff'}}>{this.state.infants}</Text>
      <TouchableOpacity onPress={() => this.setState({infants:this.state.infants-1})}>
      <Icon name="expand-more" color="#fff" size={25} />
      </TouchableOpacity>
      </View>
      </View>
      </View>
      <View style={{flex:1, margin:1, flexDirection:'row', backgroundColor:'#555', justifyContent:'space-between', }}>
      {this.seatType(seatTypes[0])}
      {this.seatType(seatTypes[1])}
      {this.seatType(seatTypes[2])}


      </View>
      <TouchableOpacity onPress={() => this.props.navigator.push({id:'select'})} style={{flex:1, margin:1, flexDirection:'row', justifyContent:'center', alignItems:'center', backgroundColor:'#24a4d3' }}>
            <Text style={{color:'#fff', fontSize:14}}>SEARCH</Text>
            </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  eachCat:{
    flexDirection:'row',
    alignItems:'center',
    flex:1,
    justifyContent:'space-around'

  },main:{
    color:"#fff",
    fontWeight:'200',
    fontSize:13
  },
  catSub:{
    color:"#a7a7a7",
    fontSize:11
  },
  catName:{

  },
  container: {
    flex: 1,
    backgroundColor:'#444'


  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
