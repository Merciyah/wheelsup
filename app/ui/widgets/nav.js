
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  View,
  StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Location extends Component {
  constructor(props){
    super(props)

    this.state = {
      to:"",
      from:'',
    }
  }


  render() {
    return (
      <View style={{height:60, margin:1, flexDirection:'row', padding:10, paddingBottom:0, backgroundColor:'#222', justifyContent:'space-between', alignItems:'center'}} >
      <Icon name="sort" color="#eee" size={25} />
      <Text style={{color:'#fff', fontWeight:'300', fontSize:14, textAlign:'center'}}>{this.props.name}</Text>
      <View style={{width:25, height:25}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#444'


  }
});
