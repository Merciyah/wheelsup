
import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,

} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Location extends Component {
  constructor(props){
    super(props)

    this.state = {
      to:"",
      from:'',
    }
  }


  render() {
    return (
      <TouchableOpacity style={{height:50, flexDirection:'row', paddingBottom:0, backgroundColor:'#24a4d3', justifyContent:'center', alignItems:'center'}} >

      <Text style={{color:'#fff', fontWeight:'400', fontSize:14, textAlign:'center'}}>{this.props.name}</Text>

      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#444'


  }
});
