import React, { Component, PropTypes } from 'react';
import {
  Navigator,
  View,
  Dimensions
} from 'react-native';


import Splash from './splash';
import Search from './search';
import Select from './select';
import Home from './home';
import Login from './login';
import Location from './location';

const routes = {
  splash: Splash,
  home: Home,
  login: Login,
  location: Location,
  search: Search,
  select: Select,

};

export default class index extends Component {


  constructor(){
    super();
    this.state = {
      departures: "",
      destination:"",
      next: {short:"destination",
          }}
    }

  setDestination(x){
     this.setState({
      destination: x
    })
  }
  setDepartures(x){
    this.setState({
      departures: x,
      next: x
    })
  }

  renderScene({id}, navigator){
    const Scene = routes[id]
    return <Scene {...this.props} next={this.state.next} setDest = {(dest) => this.setDestination(dest)} setDept = {(dept) => this.setDepartures(dept)} departures = {this.state.departures} destination ={this.state.destination} navigator={navigator} />
  }


  render(){
    return (
        <View style={{flex:1}}>
            <Navigator
              style={{flex: 1}}
              ref={'NAV'}
              initialRoute={{id: 'login', name: 'login'}}
              renderScene={this.renderScene.bind(this)}
            />
        </View>
    )
  }
}
