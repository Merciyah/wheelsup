const cities = [{
	id:1,
	name: "New York",
	country: "USA",
	image: require('../assets/images/newyork.jpg'),
	short: "JFK"
},
{
	id:2,
	name: "Amsterdam",
	country: 'Nethalands',
	image: require('../assets/images/amsterdam.jpg'),
	short: "AMS"
},
{
	id:3,
	name: "Medellin",
	country:'Colombia',
	image: require('../assets/images/mdellin.jpg'),
	short: "MDE"
},{
	id:4,
	name: "Paris",
	country:'France',
	image: require('../assets/images/paris.jpg'),
	short: "CDG"
},{
	id:4,
	name: "Santa Barbara",
	country:'USA',
	image: require('../assets/images/barbara.jpg'),
	short: "SBA"
},

]

export default cities;